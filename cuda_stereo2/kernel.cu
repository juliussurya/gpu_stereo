
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <iostream>
#include <stdio.h>
#include <cmath>

typedef unsigned int uint;
using namespace std;

extern  "C" cudaError_t addWithCuda(int *c, const int *a, const int *b, unsigned int size);
extern "C" void cudaStereoMatching(dim3 block, dim3 grid, int *left, int *right, int *out, int *mask,  int row, int col, int d_range, int win_sz, string param);

__global__ void addKernel(int *c, const int *a, const int *b)
{
    int i = threadIdx.x;
    c[i] = a[i] + b[i];
}

__global__ void matchSAD(int *left, int *right, int *out, int *mask, int row, int col, int win_sz, int d_range) {

	int x = blockIdx.x * blockDim.x + threadIdx.x;
	int y = blockIdx.y * blockDim.y + threadIdx.y;
	int idx = y * col + x;
	int z = blockIdx.z * blockDim.z + threadIdx.z;
	//int z_idx = idx + (row * col) * z;

	int l_idx, r_idx, d_idx;
	int temp, sum = 0;

	//out[idx] = left[idx];

	int xstart = d_range - 1;
	int xend =  col - win_sz;
	int ystart = 0;
	int yend = row - win_sz - 1;

	if (x >= xstart && y >= ystart && x < xend && y < yend ) {
		int min = 20000;
		for (int r = 0; r < d_range; r++) {
			//d_idx = idx + (row * col) * z ;
			// window matching
			mask[idx] = 0;

			//out[idx] = left[idx];

			//// Window matching (non loop-- manual) [5 x 5] --> slightly faster 0.2 - 1 ms
			//// 1st row
			//mask[idx] += abs(left[(y + 0) * col + (x + 0)] - right[(y + 0) * col + (x + 0) - r]);
			//mask[idx] += abs(left[(y + 0) * col + (x + 1)] - right[(y + 0) * col + (x + 1) - r]);
			//mask[idx] += abs(left[(y + 0) * col + (x + 2)] - right[(y + 0) * col + (x + 2) - r]);
			//mask[idx] += abs(left[(y + 0) * col + (x + 3)] - right[(y + 0) * col + (x + 3) - r]);
			//mask[idx] += abs(left[(y + 0) * col + (x + 4)] - right[(y + 0) * col + (x + 4) - r]);
			//mask[idx] += abs(left[(y + 0) * col + (x + 5)] - right[(y + 0) * col + (x + 5) - r]);
			//mask[idx] += abs(left[(y + 0) * col + (x + 6)] - right[(y + 0) * col + (x + 6) - r]);

			//// 2nd row
			//mask[idx] += abs(left[(y + 1) * col + (x + 0)] - right[(y + 1) * col + (x + 0) - r]);
			//mask[idx] += abs(left[(y + 1) * col + (x + 1)] - right[(y + 1) * col + (x + 1) - r]);
			//mask[idx] += abs(left[(y + 1) * col + (x + 2)] - right[(y + 1) * col + (x + 2) - r]);
			//mask[idx] += abs(left[(y + 1) * col + (x + 3)] - right[(y + 1) * col + (x + 3) - r]);
			//mask[idx] += abs(left[(y + 1) * col + (x + 4)] - right[(y + 1) * col + (x + 4) - r]);
			//mask[idx] += abs(left[(y + 1) * col + (x + 5)] - right[(y + 1) * col + (x + 5) - r]);
			//mask[idx] += abs(left[(y + 1) * col + (x + 6)] - right[(y + 1) * col + (x + 6) - r]);

			//// 3rd row
			//mask[idx] += abs(left[(y + 2) * col + (x + 0)] - right[(y + 2) * col + (x + 0) - r]);
			//mask[idx] += abs(left[(y + 2) * col + (x + 1)] - right[(y + 2) * col + (x + 1) - r]);
			//mask[idx] += abs(left[(y + 2) * col + (x + 2)] - right[(y + 2) * col + (x + 2) - r]);
			//mask[idx] += abs(left[(y + 2) * col + (x + 3)] - right[(y + 2) * col + (x + 3) - r]);
			//mask[idx] += abs(left[(y + 2) * col + (x + 4)] - right[(y + 2) * col + (x + 4) - r]);
			//mask[idx] += abs(left[(y + 2) * col + (x + 5)] - right[(y + 2) * col + (x + 5) - r]);
			//mask[idx] += abs(left[(y + 2) * col + (x + 6)] - right[(y + 2) * col + (x + 6) - r]);

			//// 4th row
			//mask[idx] += abs(left[(y + 3) * col + (x + 0)] - right[(y + 3) * col + (x + 0) - r]);
			//mask[idx] += abs(left[(y + 3) * col + (x + 1)] - right[(y + 3) * col + (x + 1) - r]);
			//mask[idx] += abs(left[(y + 3) * col + (x + 2)] - right[(y + 3) * col + (x + 2) - r]);
			//mask[idx] += abs(left[(y + 3) * col + (x + 3)] - right[(y + 3) * col + (x + 3) - r]);
			//mask[idx] += abs(left[(y + 3) * col + (x + 4)] - right[(y + 3) * col + (x + 4) - r]);
			//mask[idx] += abs(left[(y + 3) * col + (x + 5)] - right[(y + 3) * col + (x + 5) - r]);
			//mask[idx] += abs(left[(y + 3) * col + (x + 6)] - right[(y + 3) * col + (x + 6) - r]);

			//// 5th row
			//mask[idx] += abs(left[(y + 4) * col + (x + 0)] - right[(y + 4) * col + (x + 0) - r]);
			//mask[idx] += abs(left[(y + 4) * col + (x + 1)] - right[(y + 4) * col + (x + 1) - r]);
			//mask[idx] += abs(left[(y + 4) * col + (x + 2)] - right[(y + 4) * col + (x + 2) - r]);
			//mask[idx] += abs(left[(y + 4) * col + (x + 3)] - right[(y + 4) * col + (x + 3) - r]);
			//mask[idx] += abs(left[(y + 4) * col + (x + 4)] - right[(y + 4) * col + (x + 4) - r]);
			//mask[idx] += abs(left[(y + 4) * col + (x + 5)] - right[(y + 4) * col + (x + 5) - r]);
			//mask[idx] += abs(left[(y + 4) * col + (x + 6)] - right[(y + 4) * col + (x + 6) - r]);

			//// 6 th row
			//mask[idx] += abs(left[(y + 5) * col + (x + 0)] - right[(y + 5) * col + (x + 0) - r]);
			//mask[idx] += abs(left[(y + 5) * col + (x + 1)] - right[(y + 5) * col + (x + 1) - r]);
			//mask[idx] += abs(left[(y + 5) * col + (x + 2)] - right[(y + 5) * col + (x + 2) - r]);
			//mask[idx] += abs(left[(y + 5) * col + (x + 3)] - right[(y + 5) * col + (x + 3) - r]);
			//mask[idx] += abs(left[(y + 5) * col + (x + 4)] - right[(y + 5) * col + (x + 4) - r]);
			//mask[idx] += abs(left[(y + 5) * col + (x + 5)] - right[(y + 5) * col + (x + 5) - r]);
			//mask[idx] += abs(left[(y + 5) * col + (x + 6)] - right[(y + 5) * col + (x + 6) - r]);

			//// 7th row
			//mask[idx] += abs(left[(y + 6) * col + (x + 0)] - right[(y + 6) * col + (x + 0) - r]);
			//mask[idx] += abs(left[(y + 6) * col + (x + 1)] - right[(y + 6) * col + (x + 1) - r]);
			//mask[idx] += abs(left[(y + 6) * col + (x + 2)] - right[(y + 6) * col + (x + 2) - r]);
			//mask[idx] += abs(left[(y + 6) * col + (x + 3)] - right[(y + 6) * col + (x + 3) - r]);
			//mask[idx] += abs(left[(y + 6) * col + (x + 4)] - right[(y + 6) * col + (x + 4) - r]);
			//mask[idx] += abs(left[(y + 6) * col + (x + 5)] - right[(y + 6) * col + (x + 5) - r]);
			//mask[idx] += abs(left[(y + 6) * col + (x + 6)] - right[(y + 6) * col + (x + 6) - r]);

			//// window matching loop()
			//for (int i = 0; i < win_sz; i++) {
			//	for (int j = 0; j < win_sz; j++) {
			//		l_idx = (y + i) * col + (x + j);
			//		r_idx = (y + i) * col + (x + j) - r;
			//		mask[idx] += (abs(left[l_idx] - right[r_idx]));
			//	}
			//}

			//if (mask[idx] < min) {
			//	min = mask[idx];
			//	out[idx] = r ;
			//}

			//out[idx] = d_vol[d_idx];
			//// Find min
			//if (r != 0) {
			//	mask[idx] = 0;
			//	if (d_vol[z_idx] < d_vol[d_idx]) {
			//		mask[idx] = 1;
			//		mask[idx] = mask[idx] * d_vol[z_idx];
			//	}
			//	if (d_vol[d_idx] < d_vol[z_idx]) {
			//		out[idx] = r;
			//		mask[idx] = d_vol[d_idx];
			//	}
			//	d_vol[z_idx] = mask[idx];
			//}
			//else if (r == 0) {
			//	d_vol[z_idx] = d_vol[d_idx];
			//	//out[idx] = d_vol[d_idx];
			//}
			//else {

			//}

		}
	}
	//z_idx = idx + row * col * 0;
	//out[idx] = d_vol[z_idx];


}

__global__ void matchSSD(int *left, int *right, int *out, int *mask, int row, int col, int win_sz, int d_range) {

	int x = blockIdx.x * blockDim.x + threadIdx.x;
	int y = blockIdx.y * blockDim.y + threadIdx.y;
	int idx = y * col + x;
	int z = blockIdx.z * blockDim.z + threadIdx.z;
	int z_idx = idx + (row * col) * z;

	int l_idx, r_idx, d_idx;
	int temp;
	uint sum = 0;

	//out[idx] = left[idx];

	int xstart = d_range - 1;
	int xend = col - win_sz;
	int ystart = 0;
	int yend = row - win_sz - 1;

	if (x >= xstart && y >= ystart && x < xend && y < yend) {

		// window matching loop()
		for (int i = 0; i < win_sz; i++) {
			for (int j = 0; j < win_sz; j++) {
				l_idx = (y + i) * col + (x + j);
				r_idx = (y + i) * col + (x + j) - z;

				sum += ((left[l_idx] - right[r_idx]) * (left[l_idx] - right[r_idx]));
				//sum += (abs(left[l_idx] - right[r_idx]));
			}
		}
		mask[y * d_range * col + x * d_range + z] = sum;
	}

	int minval = 100000000;
	for (int d = 0; d < d_range; d++) {
		d_idx = y * d_range * col + x * d_range + d;
		if (mask[d_idx] < minval) {
			minval = mask[d_idx];
			out[idx] = d;
		}
	}
}






void cudaStereoMatching(dim3 block, dim3 grid, int *cu_left, int *cu_right, int *cu_out, int *mask, int row,  int col, int d_range, int win_sz, string param) {

	if (param == "SSD") {
		matchSSD <<< grid, block >>> (cu_left, cu_right, cu_out, mask, row, col, win_sz, d_range);
	}


}


// Helper function for using CUDA to add vectors in parallel.
cudaError_t addWithCuda(int *c, const int *a, const int *b, unsigned int size)
{
    int *dev_a = 0;
    int *dev_b = 0;
    int *dev_c = 0;
    cudaError_t cudaStatus;

    // Choose which GPU to run on, change this on a multi-GPU system.
    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        goto Error;
    }

    // Allocate GPU buffers for three vectors (two input, one output)    .
    cudaStatus = cudaMalloc((void**)&dev_c, size * sizeof(int));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    cudaStatus = cudaMalloc((void**)&dev_a, size * sizeof(int));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    cudaStatus = cudaMalloc((void**)&dev_b, size * sizeof(int));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    // Copy input vectors from host memory to GPU buffers.
    cudaStatus = cudaMemcpy(dev_a, a, size * sizeof(int), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

    cudaStatus = cudaMemcpy(dev_b, b, size * sizeof(int), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

    // Launch a kernel on the GPU with one thread for each element.
    addKernel<<<1, size>>>(dev_c, dev_a, dev_b);

    // Check for any errors launching the kernel
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "addKernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
        goto Error;
    }

    // cudaDeviceSynchronize waits for the kernel to finish, and returns
    // any errors encountered during the launch.
    cudaStatus = cudaDeviceSynchronize();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel!\n", cudaStatus);
        goto Error;
    }

    // Copy output vector from GPU buffer to host memory.
    cudaStatus = cudaMemcpy(c, dev_c, size * sizeof(int), cudaMemcpyDeviceToHost);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

Error:
    cudaFree(dev_c);
    cudaFree(dev_a);
    cudaFree(dev_b);

    return cudaStatus;
}
