#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <omp.h>

using namespace std;
using namespace cv;

typedef unsigned int uint;

extern "C" cudaError_t addWithCuda(int *c, const int *a, const int *b, unsigned int size);
extern "C" void cudaStereoMatching(dim3 block, dim3 grid, int *left, int *right, int *out, int *mask, int row, int col, int d_range, int win_sz, string param);

#define basic_image   0;
#define sequence_images 1;

const int win_sz = 7;
const int d_range = 32;
const int tx = 16;
const int ty = 16;
const int tz = 4;

void stereoMatchImageGPU(string path1, string path2, string param);
void stereoMatchImage(string path1, string path2, string param);

void stereoMatchVidGPU(string path1, string path2, string temp, string param);
void stereoMatchVid(string path1, string path2, string temp, string param);

void serialStereo(Mat output, int *arr_output, int *arr_left, int *arr_right, int *block_res, int *d_volume, int row, int col);

int main() {


	cout << "Parallel processing " << endl;
	cout << "window size : " << win_sz << " || " << "Search range : " << d_range << endl;


	string vid_path_left = "data/left/I1_%06d.png";
	string vid_path_right = "data/right/I2_%06d.png";
	string vid_path_temp = "data/left/I1_000000.png";

	string im_path_left = "img/cones/im2.ppm";
	string im_path_right = "img/cones/im4.ppm";


	stereoMatchVid(vid_path_left, vid_path_right, vid_path_temp, "SSD");
	stereoMatchVidGPU(vid_path_left, vid_path_right, vid_path_temp, "SSD");

	stereoMatchImageGPU(im_path_left, im_path_right, "SSD");
	stereoMatchImage(im_path_left, im_path_right, "SSD");

	return 0;
}

void stereoMatchImageGPU(string path_left, string path_right, string param){
	// Load image
	Mat left, right;
	Mat im_left = imread(path_left);
	Mat im_right = imread(path_right);

	VideoCapture cam_capture;

	// Convert to grayscale
	cvtColor(im_right, right, CV_BGR2GRAY);
	cvtColor(im_left, left, CV_BGR2GRAY);


	int row = left.rows;
	int col = left.cols;


	cout << row << " || " << col << endl;


	// Intialize array for image
	int *arr_left = new int[row * col];
	int *arr_right = new int[row * col];
	int *arr_output = new int[row * col];

	// Copy image data to array
	for (int i = 0; i < row; i++) {
		for (int j = 0; j < col; j++) {
			arr_left[i * col + j] = (left.at<uchar>(i, j));
			arr_right[i * col + j] = right.at<uchar>(i, j);
		}
	}

	int *cu_left = 0;
	int *cu_right = 0;
	int *cu_out = 0;
	int *d_volume = 0;
	int *mask = 0;
	//int d_range = 32;
	cudaMalloc((void**)&cu_left, row * col * sizeof(int));
	cudaMalloc((void**)&cu_right, row * col * sizeof(int));
	cudaMalloc((void**)&cu_out, row * col * sizeof(int));
	cudaMalloc((void**)&d_volume, row * col * d_range * sizeof(int));
	cudaMalloc((void**)&mask, row * col * d_range * sizeof(int));


	dim3 block(tx, ty, tz);
	dim3 grid(col / block.x, row / block.y, d_range / block.z);

	double t = (double)getTickCount();
	cudaMemcpy(cu_left, arr_left, row * col * sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(cu_right, arr_right, row * col * sizeof(int), cudaMemcpyHostToDevice);

	cudaStereoMatching(block, grid, cu_left, cu_right, cu_out, mask, row, col, d_range, win_sz, param);

	cudaMemcpy(arr_output, cu_out, row * col * sizeof(int), cudaMemcpyDeviceToHost);
	double tproc = 1000 * ((double)getTickCount() - t) / getTickFrequency();
	cout << "Processing time : " << tproc << " ms" << endl;
	cout << "finished" << endl;

	cudaFree(cu_left);
	cudaFree(cu_right);
	cudaFree(cu_out);
	cudaFree(d_volume);
	cudaFree(mask);

	// Copy image data from array to mat
	Mat im_out = Mat(row, col, CV_8UC1);
	for (int i = 0; i < row; i++) {
		for (int j = 0; j < col; j++) {
			im_out.at<uchar>(i, j) = arr_output[i * col + j];
			//cout << arr_output[i * col + j] << endl;
			//cin.get();
		}
	}
	im_out = (im_out / d_range) * 255;
	imshow("Output", im_out);
	waitKey(0);



}

void stereoMatchVidGPU(string im_path_left, string im_path_right, string im_path_temp, string param) {

	// Load temp value for row and col
	Mat im_temp = imread(im_path_temp);
	int row = im_temp.rows;
	int col = im_temp.cols;

	cout << row << " || " << col << endl;

	VideoCapture seq_left(im_path_left);
	VideoCapture seq_right(im_path_right);


	Mat im_l, im_r, im_out;

	double fstart, fend, fproc, fps;

	//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	// Allocate memory in GPU
	int *cu_left = 0;
	int *cu_right = 0;
	int *cu_out = 0;
	int *d_volume = 0;
	int *mask = 0;
	//int d_range = 32;
	cudaMalloc((void**)&cu_left, row * col * sizeof(int));
	cudaMalloc((void**)&cu_right, row * col * sizeof(int));
	cudaMalloc((void**)&cu_out, row * col * sizeof(int));
	cudaMalloc((void**)&d_volume, row * col * d_range * sizeof(int));
	cudaMalloc((void**)&mask, row * col * d_range * sizeof(int));

	dim3 block(tx, ty, tz);
	dim3 grid(col / block.x, row / block.y, d_range / block.z);
	//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


	for (;;) {

		fstart = omp_get_wtime();

		seq_left >> im_l;
		seq_right >> im_r;

		if (!im_l.empty() && !im_r.empty()) {

			// Put image in array
			// Intialize array for image
			int *arr_left = new int[row * col];
			int *arr_right = new int[row * col];
			int *arr_output = new int[row * col];

			// Copy image data to array
			for (int i = 0; i < row; i++) {
				for (int j = 0; j < col; j++) {
					arr_left[i * col + j] = im_l.at<uchar>(i, j);
					arr_right[i * col + j] = im_r.at<uchar>(i, j);
				}
			}

			//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
			// GPU Processing
			//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
			cudaMemcpy(cu_left, arr_left, row * col * sizeof(int), cudaMemcpyHostToDevice);
			cudaMemcpy(cu_right, arr_right, row * col * sizeof(int), cudaMemcpyHostToDevice);

			cudaStereoMatching(block, grid, cu_left, cu_right, cu_out, mask, row, col, d_range, win_sz, param);

			cudaMemcpy(arr_output, cu_out, row * col * sizeof(int), cudaMemcpyDeviceToHost);
			//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
			//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

			// Copy image data from array to mat
			Mat im_out = Mat(row, col, CV_8UC1);
			for (int i = 0; i < row; i++) {
				for (int j = 0; j < col; j++) {
					im_out.at<uchar>(i, j) = arr_output[i * col + j];
					//cout << arr_output[i * col + j] << endl;
					//cin.get();
				}
			}

			im_out = (im_out / d_range) * 255;

			fend = omp_get_wtime();
			fproc = fend - fstart;
			fps = (1 / fproc);

			putText(im_l, "FPS : " + to_string(fps), Point(10, 20), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 255, 255), 1);

			namedWindow("Image left");
			namedWindow("Image right");
			namedWindow("Output");
			imshow("Image left", im_l);
			imshow("Image right", im_r);
			imshow("Output", im_out);

			waitKey(10);

		}
		else {
			destroyAllWindows();
			break;
		}
	}
}

void stereoMatchImage(string im_path_left, string im_path_right, string param) {
	Mat left, right;
	Mat im_left = imread(im_path_left);
	Mat im_right = imread(im_path_right);

	Mat output_im = Mat(im_left.rows, im_left.cols, CV_8UC1);

	cvtColor(im_left, left, CV_BGR2GRAY);
	cvtColor(im_right, right, CV_BGR2GRAY);

	// Get image size
	int im_row = im_left.rows;
	int im_col = im_left.cols;

	// Array initalization
	int *arr_left = new int[im_row * im_col];
	int *arr_right = new int[im_row * im_col];
	int *arr_output = new int[im_row * im_col];
	int *block_res = new int[win_sz * win_sz];
	int *d_volume = new int[im_row * im_col * d_range];
	memset(arr_left, 0, sizeof(int) * im_row * im_col);
	memset(arr_right, 0, sizeof(int) * im_row * im_col);
	memset(arr_output, 0, sizeof(int) * im_row * im_col);
	memset(block_res, 0, sizeof(int) * win_sz * win_sz);
	memset(d_volume, 0, sizeof(int) * im_row * im_col * d_range);

	int *temp_left = block_res;
	int *temp_right = block_res;

	// Copy image data to array
	for (int i = 0; i < im_row; i++) {
		for (int j = 0; j < im_col; j++) {
			arr_left[i * im_col + j] = (int)left.at<uchar>(i, j);
			arr_right[i * im_col + j] = (int)right.at<uchar>(i, j);
		}
	}
	//cout << arr_left[0] << endl;
	//cout << arr_right[0] << endl;




	cout << "Stereo matching starting . . . ";
	// Stereo matching
	//tic();
	double t = (double)getTickCount();
	serialStereo(output_im, arr_output, arr_left, arr_right, block_res, d_volume, im_row, im_col);
	double tproc = 1000 * ((double)getTickCount() - t) / getTickFrequency();
	cout << tproc << endl;
	cout << "finished" << endl;
	//toc();

	// 1. Normalize value first
	Mat output_cmp = (output_im / d_range) * 255; //(better methods are welcomed)

	namedWindow("Image output");
	namedWindow("Image left");
	namedWindow("Image right");
	imshow("Image left", im_left);
	imshow("Image right", im_right);
	imshow("Image output", output_cmp);
	waitKey(0);
}

void stereoMatchVid(string im_path_left, string im_path_right, string im_path_temp, string param) {
	Mat im_temp = imread(im_path_temp);
	int row = im_temp.rows;
	int col = im_temp.cols;

	cout << row << " || " << col << endl;

	VideoCapture seq_left(im_path_left);
	VideoCapture seq_right(im_path_right);


	Mat im_l, im_r;
	Mat im_out = Mat(row, col, CV_8UC1);

	double fstart, fend, fproc, fps;



	for (;;) {

		fstart = omp_get_wtime();

		seq_left >> im_l;
		seq_right >> im_r;

		if (!im_l.empty() && !im_r.empty()) {

			// Put image in array
			// Intialize array for image
			int *arr_left = new int[row * col];
			int *arr_right = new int[row * col];
			int *arr_output = new int[row * col];
			int *block_res = new int[win_sz * win_sz];
			int *d_volume = new int[row * col * d_range];

			// Copy image data to array
			for (int i = 0; i < row; i++) {
				for (int j = 0; j < col; j++) {
					arr_left[i * col + j] = im_l.at<uchar>(i, j);
					arr_right[i * col + j] = im_r.at<uchar>(i, j);
				}
			}

			serialStereo(im_out, arr_output, arr_left, arr_right, block_res, d_volume, row, col);


			im_out = (im_out / d_range) * 255;

			fend = omp_get_wtime();
			fproc = fend - fstart;
			fps = (1 / fproc);

			putText(im_l, "FPS : " + to_string(fps), Point(10, 20), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 255, 255), 1);

			namedWindow("Image left");
			namedWindow("Image right");
			namedWindow("Output");
			imshow("Image left", im_l);
			imshow("Image right", im_r);
			imshow("Output", im_out);

			waitKey(10);

		}
		else {
			destroyAllWindows();
			break;
		}
	}
}

void serialStereo(Mat output_im, int *arr_output, int *arr_left, int *arr_right, int *block_res, int *d_volume, int im_row, int im_col) {

	// Index for looping
	int start_row = 0;
	int start_col = d_range - 1;
	int end_row = im_row - win_sz - 1;
	int end_col = im_col - win_sz - 1;
	int start_search = 0;
	int end_search = start_search + d_range - 1;

	for (int i = start_row; i < end_row; i++) {
		for (int j = start_col; j < end_col; j++) {
			// 3 x 3 block operation
			// current pixel index
			int curr_px = i * im_col + j;
			//cout << curr_px << endl;

			// Search range
			int min_disp = 1000000;
			int disparity = d_range;

			// Search to the left from right image
			for (int range = start_search; range < d_range; range++) {
				int block_sum = 0;
				for (int x = 0; x < win_sz; x++) {
					for (int y = 0; y < win_sz; y++) {
						// current pixel index on block operation
						int left_idx = curr_px + (x * im_col) + y;
						int right_idx = curr_px - range + (x * im_col) + y;
						block_res[x * win_sz + y] = abs(arr_right[right_idx] - arr_left[left_idx]); // stored in block x block array
						block_sum += block_res[x * win_sz + y];

					}
				}
				d_volume[curr_px + range] = block_sum;

				// Select the lowest SAD value
				if (block_sum < min_disp) {
					min_disp = block_sum;
					disparity = range;
					//disparity = min_disp;
				}
			} // search range loop
			  //cout << disparity << endl;
			arr_output[curr_px] = disparity;
			output_im.at<uchar>(i + win_sz / 2, j + win_sz / 2) = disparity;
		} // col loop
	} // row loop
}